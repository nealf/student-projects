# Student Projects

A list of potential project work in no particular order

## End-to-end website testing
Work with the Playwright framework to develop, from scratch, end-to-end testing that can be used on applications at VTTI. Will create a template and docs to assist other developers in creating their own tests. Will involve interacting with our authentication systems and CI/CD. Playwright supports writing tests in Javascript, Python, Java, and C#.

## Dashboards and Monitoring
Work with a Grafana instance to migrate and create new dashboards for applications and servers across VTTI. We’re revamping our Grafana system and want to leverage more dashboards-as-code tools in the process (to be decided which ones). In addition to working with Grafana directly, this will require work with data sources like Prometheus, Loki, and Zabbix. There is also the possibility of developing and working with Service Level Objectives (SLOs) and alerting.

## Logging authorization proxy
We leverage the Grafana Loki project for application logs and are looking to revamp and expand its use across VTTI. We need to develop an HTTP proxy that can provide authorization decisions based on the user’s groups and roles. While implementation details are not decided yet, we’re looking to leverage the Open Policy Agent for this project.

## Tracing
We’re interested in implementing OpenTelemetry Tracing in a number of projects and applications (using Grafana Tempo for storage). This would be a new capability for VTTI and requires documentation and examples of how to leverage this tool from adding to your application to visualizing and searching in Grafana.

## Backstage.io implementation
Backstage is a developer platform tool used for cataloging software and services across your organization. We are planning to roll this out and need help inventorying our software, importing it into Backstage, and configuring associated plugins.

## Keycloak authentication projects
Keycloak is our preferred authentication system, making OIDC and SAML authentication available to applications. It is primarily backed by our Active Directory, but can also allow VT accounts and logins through other Identity Providers (IdP). There are a number of projects we’d like to implement around Keycloak including migrating to a newer Kubernetes deployment method and writing a plugins to send events to a data streaming system (Pulsar/Kafka), managing imported group names, and new themes. Java, HTML

## Apache Superset work
Apache Superset is an open-source data visualization platform (sort of like Tableau) that provides a mechanism to explore connected data sources via SQL and create charts and dashboards from the results. There are a couple of projects here, one is building a map-based visualization plugin using React and Typescript along with some basic GIS and mapping tools, while the other is working with the Flask App Builder in Python to develop an authorization system that scales to our user environment.

## Helm charts
VTTI uses Helm to manage most of our applications deployed to our Kubernetes environment. We are looking at a number of ways to upgrade that experience and improve some of the Helm charts we use with new capabilities.

## CI/CD workflow improvements
We use GitLab’s CI/CD system along with GitOps (via FluxCD) for deployments to our Kubernetes environment. There are a number of improvements to be made here, from security to ease of use and observability during and after deployment.

## Translate file system ACLs to OPA policies
We use network file systems that have complex Access Control Lists (ACLs) for permissions on directories, sub-directories, and files. We need to translate these to Open Policy Agent policies written in Rego so that we can leverage them across multiple other projects including being able to access them via object storage protocols like S3.

## Using Apache Camel for data services
VTTI has begun using Apache Camel for a number of projects and plans to use it much more frequently in the future. We have a number of legacy applications that could be replaced by apps built using Camel. This would also help provide more examples and templates for other projects. We may also look to leverage the Camel K project (and Kamelets and Karavan) for faster, simpler deployment on Kubernetes.

## Hashicorp Boundary deployment
We want to deploy Hashicorp Boundary to provide more secure access to services within our network and Kubernetes clusters. This would involve deploying Boundary and its configuration using Terraform as well as providing documentation and training materials on adding services and its use by clients.

## GIS Services
VTTI has a lot of spatial data, but limited ways to map and analyze it in easily accessible ways. There are a number of open-source tools and projects we would like to leverage to provide basic GIS capabilities to our researchers and analysts, and this task would involve selecting those, deploying them, and providing basic documentation on their use.

## Website tracking implementation
There are data security and usage requirements that make using hosted services like Google Analytics difficult for a number of our projects. We’d like to implement Plausible Analytics for lightweight, basic use-cases and PostHog where more detailed analysis is desired.

## Deploy KubeFlow for Machine Learning pipelines
VTTI wants to help researchers better access our GPU resources and provide more streamlined workflows for their machine learning and data processing pipelines. We plan to test out KubeFlow and potentially other associated projects as part of that platform. This would involve helping to deploy and configure the components as well as creating documentation and training materials.

## Test Kubernetes development tools
As we move to more microservices, developers at VTTI need tools and workflows to facilitate development with services running in Kubernetes. Tools like Telepresence and Gefyra provide ways to integrate and proxy traffic from local development machines to services running in Kubernetes. Deploying these securely requires configuration, testing, and documentation.
